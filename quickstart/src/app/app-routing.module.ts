import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';


import { UsersComponent } from './user/users.component';
import { UserDetailComponent } from './user/user-detail.component';

import { ItemFoundDetailComponent } from './item/item-found-detail.component';
import { ItemLostDetailComponent } from './item/item-lost-detail.component';
import { ItemsFoundComponent } from './item/items-found.component';
import { ItemsLostComponent } from './item/items-lost.component';
import { ItemAddFoundComponent } from './item/item-found-add.component';
import { ItemAddLostComponent } from './item/item-lost-add.component';

import { WishItemsComponent } from './wishItem/wishItems.component';     //do poprawy nazwa. barkuje myslinika
import { WishItemDetailComponent } from './wishItem/wish-item-detail.component';

import { HistoryItemsComponent } from './history-item/component/history-items.component'


const routes: Routes = [
  { path: '', redirectTo: '/wishItems', pathMatch: 'full' },
  { path: 'user/:id', component: UserDetailComponent },
  { path: 'itemFound/:id', component: ItemFoundDetailComponent },
  { path: 'itemLost/:id', component: ItemLostDetailComponent },
  { path: 'wishItem/:id', component: WishItemDetailComponent },
  { path: 'users', component: UsersComponent },
  { path: 'itemsFound', component: ItemsFoundComponent },
  { path: 'itemsLost', component: ItemsLostComponent },
  { path: 'wishItems', component: WishItemsComponent },
  { path: 'addFoundItem', component: ItemAddFoundComponent },
  { path: 'addLostItem', component: ItemAddLostComponent },
  { path: 'historyItems', component: HistoryItemsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes), FormsModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
