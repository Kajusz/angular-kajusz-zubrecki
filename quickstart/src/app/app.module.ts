import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';


import { AppRoutingModule } from './app-routing.module';

// Imports for loading & configuring the in-memory web api
import { InMemoryWebApiModule } from 'angular-in-memory-web-api'; // mock srwis dopki nie mamy web serwera
import { InMemoryDataService } from './in-memory-data.service';

import { AppComponent } from './app.component';

import { UserService } from './user/user.service';
import { UsersComponent } from './user/users.component';
import { UserDetailComponent } from './user/user-detail.component';
import { UserSearchComponent } from './user/user-search.component';


import { ItemService } from './item/item.service';
import { ItemsLostComponent } from './item/items-lost.component';
import { ItemsFoundComponent } from './item/items-found.component';
import { ItemAddLostComponent }  from './item/item-lost-add.component';
import { ItemAddFoundComponent }  from './item/item-found-add.component';
import { ItemLostDetailComponent }  from './item/item-lost-detail.component';
import { ItemFoundDetailComponent }  from './item/item-found-detail.component';

import { WishItemService } from './wishItem/wishItem.service';
import { WishItemsComponent } from './wishItem/wishItems.component';
import { WishItemDetailComponent } from './wishItem/wish-item-detail.component';

import { HistoryItemsComponent } from './history-item/component/history-items.component';
import { HistoryItemsService } from './history-item/service/history-items.service';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    InMemoryWebApiModule.forRoot(InMemoryDataService, {passThruUnknownUrl: true}),
    AppRoutingModule,
    JsonpModule
  ],
  declarations: [
    AppComponent,

    UsersComponent,
    UserDetailComponent,
    UserSearchComponent,

    ItemAddLostComponent,
    ItemAddFoundComponent,
    ItemsLostComponent,
    ItemsFoundComponent,
    ItemFoundDetailComponent,
    ItemLostDetailComponent,
    
    WishItemsComponent,
    WishItemDetailComponent,

    HistoryItemsComponent
  ],
  providers: [UserService, ItemService, WishItemService, HistoryItemsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
