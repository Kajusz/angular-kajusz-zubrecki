import { Injectable }    from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { WishItem } from './wishItem';

import { User } from '../user/user';

import { Item } from '../item/item'

@Injectable()
export class WishItemService {

  private headers = new Headers({'Content-Type': 'application/json'});
  private wishItemsUrl = 'api/wishItems';  // URL to web api

  constructor(private http: Http) { }

  getWishItems(): Promise<WishItem[]> {
    return this.http.get(this.wishItemsUrl)
               .toPromise()
               .then(response => response.json().data as WishItem[])
               .catch(this.handleError);
  }

  getWishItem(id: number): Promise<WishItem> {
    const url = `${this.wishItemsUrl}/${id}`;
    return this.http.get(url)
      .toPromise()
      .then(response => response.json().data as WishItem)
      .catch(this.handleError);
  }

  delete(id: number): Promise<void> {
    const url = `${this.wishItemsUrl}/${id}`;
    return this.http.delete(url, {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  create(user: User[], item: Item, dateOfReservation: Date[] ): Promise<WishItem> {
    return this.http
      .post(this.wishItemsUrl, JSON.stringify({ user:user, item: item, dateOfReservation: dateOfReservation}), {headers: this.headers})
      .toPromise()
      .then(res => res.json().data as WishItem)
      .catch(this.handleError);
  }

  update(wishItem: WishItem): Promise<WishItem> {
    const url = `${this.wishItemsUrl}/${wishItem.id}`;
    return this.http
      .put(url, JSON.stringify(wishItem), {headers: this.headers})
      .toPromise()
      .then(() => wishItem)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred sdsdf', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}

