import 'rxjs/add/operator/switchMap';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

import { WishItem } from './wishItem';
import { WishItemService } from './wishItem.service';

import { User } from '../user/user';
import { UserService } from '../user/user.service';

@Component({
  selector: 'wish-item-detail',
  templateUrl: '../../html/wish-item-detail.component.html',
  styleUrls: ['../../css/wish-item-detail.component.css','../../css/modal.css' ]
})

export class WishItemDetailComponent implements OnInit {
  wishItem: WishItem;
  users: User[];
  selectedFoundUser: User[];
  indexOfelementToDelete: number;

  constructor(
    private wishItemService: WishItemService,
    private userService: UserService,
    private route: ActivatedRoute,
    private location: Location,
    private router: Router
  ) { }

  getUsers(): void {
    this.userService
      .getUsers()
      .then(users => this.users = users);

    /*         this.userService.getUsers()
            .subscribe(
              users => this.users = users,
               error =>  this.errorMessage = error); */
  }

  getWishItem(): void {
    this.wishItemService
      .getWishItem(this.wishItem.id)
      .then(wishItem => this.wishItem = wishItem);
  }

  getUser() {

  }

  ngOnInit(): void {
    this.getUsers();
    this.route.paramMap
      .switchMap((params: ParamMap) => this.wishItemService.getWishItem(+params.get('id')))
      .subscribe(wishItem => this.wishItem = wishItem);
  }

  deleteReservation(user: User) {
    if (this.wishItem.user.length === 1) {
      document.getElementById('modal-body').innerHTML = "<br><br> You can not delete last user." + "<br><br>";
      document.getElementById('myModal').style.display = "block";
      return;
    }
    this.indexOfelementToDelete = this.wishItem.user.indexOf(user);
    this.wishItem.user.splice(this.indexOfelementToDelete, 1);
    this.wishItem.dateOfReservation.splice(this.indexOfelementToDelete, 1);
    this.wishItemService.update(this.wishItem);
  }

  addWishItemToUser(user: User) {
    user.wishItems++;
    this.userService.update(user);
  }

  checkIfPossibleAndAddUserToItem(user: User) {
    if (user.wishItems === undefined) {
      user.wishItems = 0;
      return true;
    }
    if (user.wishItems > 2) {
      this.showInformation("User has already 3 items reserved");
      return false;
    }
    return true;
  }

  checkIfUserSelected(user: User) {
    if (user === undefined) {
      this.showInformation("If you want add user, you should choose one from list");
      return false;
    } else {
      return true;
    }
  }

  addUserToWishItemIfPossible(user: User) {
    if(this.checkIfUserSelected(user)){
    let conditionItems = this.checkIfPossibleAndAddUserToItem(user);
    let conditionUsers = this.checkIfUserAlreadyReservedItem(user);
    if (conditionItems && conditionUsers) {
      this.addWishItemToUser(user);
      this.addUserToWishItem(user);
    }
  }
  }

  addUserToWishItem(user: User) {
    this.wishItem.user.push(user);
    this.wishItem.dateOfReservation.push(new Date());
    this.wishItemService.update(this.wishItem);
  }

  checkIfUserAlreadyReservedItem(user: User) {
    for (var i = 0; i < this.wishItem.user.length; i++) {
      if (this.wishItem.user[i].id === user.id) {
        this.showInformation("User already on list");
        return false;
      }
    } return true;
  }

  showInformation(text: string){
    document.getElementById('modal-body').innerHTML = "<br><br>" + text + "<br><br><br>";
    document.getElementById('myModal').style.display = "block";
  }


  save(): void {
    this.wishItemService.update(this.wishItem)
      .then(() => this.goBack());
  }

  goBack(): void {
    this.location.back();
  }

  closePopUp() {
    document.getElementById('myModal').style.display = "none";
  }
}