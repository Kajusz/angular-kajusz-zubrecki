import { Item } from '../item/item';
import { User } from '../user/user';

export class WishItem {
  id: number;
  user: User[];
  item: Item;
  dateOfReservation: Date[];

}