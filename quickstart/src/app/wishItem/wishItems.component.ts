import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


import { WishItem } from './wishItem';
import { WishItemService } from './wishItem.service';

import { User } from '../user/user';
import { UserService } from '../user/user.service';

import { HistoryItemsService } from '../history-item/service/history-items.service';

import { Item } from '../item/item'
import { ItemService } from '../item/item.service';

@Component({
  selector: 'my-wish-items',
  templateUrl: '../../html/wishItems.component.html',
  styleUrls: ['../../css/wishItems.component.css']
})
export class WishItemsComponent implements OnInit {

  wishItems: WishItem[];
  users: User[];
  selectedWishItem: WishItem;
  selectedUser: User;
  userToUpdate: User;
  ownerOfWishItem: boolean = false;
  ownerOfWishItemString: string;
  daysToCollectItem: number = 30;

  
  fromAtoZ: boolean = true;
  i: number;
  j: number;
  temp: WishItem;

  constructor(
    private userService: UserService,
    private itemService: ItemService,
    private historyItemsService: HistoryItemsService,
    private wishItemService: WishItemService,
    private router: Router) { }

  getWishItems(): void {
    this.wishItemService
      .getWishItems()
      .then(wishItems => this.wishItems = wishItems);
  }

  getTimeAfterItemRegistered(selectedWishItem: Item): number {
    return Math.floor((new Date().getTime() / (1000 * 60 * 60 * 24) - new Date(selectedWishItem.when).getTime() / (1000 * 60 * 60 * 24)));
  }

  selectWishItem(selectedWishItem: WishItem) {
    if (this.checkIfPointedAmountOfDaysLeft(selectedWishItem)) {
      this.selectedWishItem = selectedWishItem;
      document.getElementById('myModal').style.display = "block";
      this.selectedUser = undefined;
    }
  }

  checkIfPointedAmountOfDaysLeft(selectedWishItem: WishItem) {
    if (this.getTimeAfterItemRegistered(selectedWishItem.item) < this.daysToCollectItem) {
      this.showInformation("30 days should left. " + this.getTimeAfterItemRegistered(selectedWishItem.item) + " left.")
      return false;
    } else {
      return true;
    }
  }

  disableTable() {
    this.ownerOfWishItem = !this.ownerOfWishItem;
    if (this.ownerOfWishItem) {
      document.getElementById('usersList').style.display = "block";
      document.getElementById('collectUserButton').style.display = "none";
      document.getElementById('collectOwnerButton').style.display = "block";
    } else {
      document.getElementById('usersList').style.display = "none";
      document.getElementById('collectUserButton').style.display = "block";
      document.getElementById('collectOwnerButton').style.display = "none";
    }
  }

  closePopUp() {
    document.getElementById('myModal').style.display = "none";
    document.getElementById('itemCollected').style.display = "none";
  }

  checkIfOwnerIsSelected() {
    if (this.selectedUser === undefined) {
      this.selectedUser = this.selectedWishItem.user[0];
      this.ownerOfWishItemString = "no";
    } else {
      this.ownerOfWishItemString = "yes";
    }
  }

  decreaseUsersWishItems(selectedWishItem: WishItem) {
    for (let i = 0; i < selectedWishItem.user.length; i++) {
      selectedWishItem.user[i].wishItems--;
      this.userService.update(selectedWishItem.user[i]);
    }
  }

  updateUsersInWishItems(selectedWishItem: WishItem) {
    for (let i = 0; i < this.wishItems.length; i++) {
      for (let j = 0; j < this.wishItems[i].user.length && this.wishItems[i] != selectedWishItem; j++) {
        for (let k = 0; k < selectedWishItem.user.length; k++) {
          if (selectedWishItem.user[k].id === this.wishItems[i].user[j].id) {
            this.wishItems[i].user[j].wishItems = selectedWishItem.user[k].wishItems;
            this.wishItemService.update(this.wishItems[i]);
          }
        }
      }
    }
  }

  showInformation(text: string) {
    document.getElementById('modal-body').innerHTML = "<br><br>" + text + "<br><br><br>";
    document.getElementById('myModal').style.display = "block";
  }

  collectItem(selectedUser: User) {
    this.checkIfOwnerIsSelected();
    this.historyItemsService.create(this.selectedWishItem.item, this.selectedUser, this.ownerOfWishItemString, new Date());
    this.closePopUp();
    this.goToHistoryItems();
    this.delete(this.selectedWishItem);
    this.itemService.delete(this.selectedWishItem.item.id, true);
  }

  delete(wishItem: WishItem): void {
    this.decreaseUsersWishItems(wishItem);
    this.updateUsersInWishItems(wishItem);
    this.wishItemService
      .delete(wishItem.id)
      .then(() => {
        this.wishItems = this.wishItems.filter(h => h !== wishItem);
        if (this.selectedWishItem === wishItem) { this.selectedWishItem = null; }
      });
  }

  getUsers(): void {
    this.userService
      .getUsers()
      .then(users => this.users = users);
  }

  getUser(id: number): void {
    this.userService
      .getUser(id)
      .then(userToUpdate => { this.userToUpdate = userToUpdate; });
  }

  ngOnInit(): void {
    this.getWishItems();
    this.getUsers();
    this.disableTable();
  }

  onSelect(wishItem: WishItem): void {
    this.selectedWishItem = wishItem;
  }

  goToHistoryItems() {
    this.router.navigate(['/historyItems']);
  }

  gotoDetail(wishItem: WishItem): void {
    this.selectedWishItem = wishItem;
    this.router.navigate(['/wishItem', this.selectedWishItem.id]);
  }

  goToWishList() {
    this.router.navigate(['/wishItems']);
  }

  sortByName(): void {

    this.fromAtoZ = !this.fromAtoZ;

    for (this.i = 0; this.i < this.wishItems.length; this.i++) {
      for (this.j = 0; this.j < this.wishItems.length - 1; this.j++) {
        if ((this.wishItems[this.j].item.name > this.wishItems[this.j + 1].item.name) && this.fromAtoZ) {
          this.temp = this.wishItems[this.j + 1];
          this.wishItems[this.j + 1] = this.wishItems[this.j];
          this.wishItems[this.j] = this.temp;
        } else if ((this.wishItems[this.j].item.name < this.wishItems[this.j + 1].item.name) && !this.fromAtoZ) {
          this.temp = this.wishItems[this.j + 1];
          this.wishItems[this.j + 1] = this.wishItems[this.j];
          this.wishItems[this.j] = this.temp;
        }
      }
    }
  }
}
