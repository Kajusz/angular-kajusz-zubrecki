import { WishItemsComponent } from '../wishItem/wishItems.component';

import { HistoryItemsService } from '../history-item/service/history-items.service';
import { WishItemService } from '../wishItem/wishItem.service';
import { UserService } from '../user/user.service';
import { ItemService } from '../item/item.service';

import { Router } from '@angular/router';
import { NgModule, DebugElement, ReflectiveInjector } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from "@angular/platform-browser";
import { ConnectionBackend, RequestOptions, BaseRequestOptions, Http } from "@angular/http";
import { MockBackend } from "@angular/http/testing";

// const userServiceSub = [{
//   id: 1,  name: 'Olivia', 
//   surname: 'Kowalski', 
//   address: "London, M. Street 3", 
//   phoneNumber: 2079460631, 
//   email: "olivia.kow@in.eu", 
//   wishlist: 0 
// } ];

describe('WishItemsComponent tests', function () {

  let component: WishItemsComponent;
  let fixture: ComponentFixture<WishItemsComponent>;
  let debugElement = DebugElement;
  let htmlElement = HTMLElement;


  
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WishItemsComponent ],
      providers: [
        { provide: WishItemService},
        { provide: UserService},
        { provide: ItemService },
        { provide: WishItemsComponent},
        { provide: HistoryItemsService},
        { provide: Router},
      ]
      , imports: [FormsModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    this.injector = ReflectiveInjector.resolveAndCreate([
      {provide: ConnectionBackend, useClass: MockBackend},
      {provide: RequestOptions, useClass: BaseRequestOptions},
      Http,
      UserService,
      ItemService,
      HistoryItemsService,
      WishItemService

    ]);
    this.userService = this.injector.get(UserService);
    this.wishItemsService = this.injector.get(WishItemService);


    this.backend = this.injector.get(ConnectionBackend) as MockBackend;
    this.backend.connections.subscribe((connection: any) => this.lastConnection = connection);
  });

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [WishItemsComponent]
    })
    fixture = TestBed.createComponent(WishItemsComponent);
    component = fixture.componentInstance;
    this.userService = fixture.debugElement.injector.get(UserService);
    this.userService = TestBed.get(UserService);

    this.wishItemService = fixture.debugElement.injector.get(WishItemService);
    this.wishItemService = TestBed.get(WishItemService);
  });

  it('should create the WishItemsComponent', async(() => {
    expect(component).toBeTruthy();
  }));


  it('should check users is defined', async(() => {
    expect(component.daysToCollectItem).toEqual(30);
  })); 

   it('should check foundItems exist ', async(() => {
    expect(component.wishItems).toEqual([]);
  })); 

  it('should check users is defined', async(() => {
    expect(component.users).toBeDefined();
  })); 


  

 
});