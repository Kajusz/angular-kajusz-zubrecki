import { InMemoryDbService } from 'angular-in-memory-web-api';

import { Item } from './item/item';
import { User } from './user/user';

export class InMemoryDataService implements InMemoryDbService {

  createDb() {
    const itemsFound = [
        {id: 0, name: 'czerwony parasol', where: "Warszawa",onWishList: true, when:         new Date("2016-01-11"), description: "Bardzo piękny  czerwony parasol firmy Umbrella"  },
        {id: 1, name: 'zielona czapka', where: "Poznań",onWishList: true, when:             new Date("2015-04-10"), description: "Zielona czapka z daszkiem firmy Hut"  },
        {id: 2, name: 'sportowy but', where: "Kraków",onWishList: false, when:               new Date("2014-05-05"), description: "But nike do grania w piłkę nożną rozmiar 36"  },
        {id: 3, name: 'złoty telefon', where: "Bolecławiec",onWishList: false, when:         new Date("2013-06-22"), description: "Złoty telefon samsung s7 edge limited edition 2017" },
        {id: 4, name: 'samochod dacia', where: "Elbląg",onWishList: false, when:             new Date("2015-08-25"), description: "Na parkingu koło tesco. Nowa Dacia. 2017" },
        {id: 5, name: 'laptop Dell', where: "Szczecin",onWishList: false, when:              new Date("2012-03-29"), description: "Laptop z 1999 roku" },
        {id: 6, name: 'piesek Pimpek', where: "Piła",onWishList: false, when:                new Date("2015-01-15"), description: "Czarny piesek rasy bulldog. Dzieci tęsknią"},
        {id: 7, name: 'okulary Rajbany', where: "Chełm", onWishList: false,when:             new Date("2014-08-05"), description: "Lekko porysowane"  },
        {id: 8, name: 'iphone X', where: "Kołobrzeg",onWishList: false, when:                new Date("2013-02-27"), description: "Nowy iphone. Jeszcze z foliami"  },
        {id: 9, name: 'plecak szkolny', where: "Mielno", onWishList: false,when:             new Date("2011-08-05"), description: "różowy plecak idąc do podstawówki"  },
        {id: 10, name: 'rękawice bokserskie', where: "Międzyzdroje",onWishList: false, when: new Date("2010-10-15"), description: "Znalezione w klubie puncher" },
        {id: 11, name: 'piłka do kosza', where: "Zacisze",onWishList: false, when:           new Date("2016-12-13"), description: "Piłka firmy addidac. Kolor żółty. " },
        {id: 12, name: '1000zł', where: "Berlin",onWishList: false, when:                    new Date("2016-02-01"), description: "..." },
        {id: 13, name: 'krzesło ', where: "Nowy York",onWishList: false, when:               new Date("2017-09-02"), description: "Znalezione koło biedronki"},
      ];




    const users = [
      { id: 0, name: 'Mieszko', surname: 'I', email: 'Mieszko@poczta.pl', wishItems: 1},
      { id: 1, name: 'Józef ', surname: 'Piłsudski ', email: 'Piłsudski@poczta.pl', wishItems:  2},
      { id: 2, name: 'Cyprian ', surname: 'Kamil Norwid ', email: 'Cyprian@poczta.pl', wishItems:  2},
      { id: 3, name: 'Juliusz ', surname: 'Słowacki ', email: 'Juliusz@poczta.pl', wishItems:  0},
      { id: 4, name: 'Zygmunt ', surname: 'Krasiński ', email: 'Zygmunt@poczta.pl', wishItems:  0},
      { id: 5, name: 'Ignacy ', surname: 'Krasicki ', email: 'Ignacy@poczta.pl', wishItems:  0},
      { id: 6, name: 'Mikołaj', surname: 'Rej', email: 'Mikołaj@poczta.pl', wishItems:  0},
      { id: 7, name: 'Mateusz', surname: 'Nowak', email: 'Mateusz@poczta.pl', wishItems:  1},
      { id: 8, name: 'Jan', surname: 'Kowalski', email: 'Kowalski@poczta.pl', wishItems:  0},
      { id: 9, name: 'Mariusz', surname: 'Pudzianowki', email: 'Pudzianowki@poczta.pl', wishItems:  0},
      { id: 10, name: 'Adam ', surname: 'Małysz ', email: 'Małysz@poczta.pl', wishItems:  0},
      { id: 11, name: 'Adam', surname: 'Mickiewicz', email: 'Mickiewicz@poczta.pl', wishItems:  0},
      { id: 12, name: 'Napolen', surname: 'Bonaparte', email: 'Bonaparte@poczta.pl', wishItems:  0},
      { id: 13, name: 'Tadeusz', surname: 'Kocisciuszko', email: 'Kocisciuszko@poczta.pl', wishItems:  0},
      { id: 14, name: 'Stefan', surname: 'Żeromski', email: 'Żeromski@poczta.pl', wishItems:  0},
      { id: 15, name: 'Fryderyk', surname: 'Szopen', email: 'Szopen@poczta.pl', wishItems:  0},
      { id: 16, name: 'Kazimierz', surname: 'Wielki', email: 'Wielki@poczta.pl', wishItems:  0},
      { id: 17, name: 'Henryk', surname: 'Sienkiewicz', email: 'Sienkiewicz@poczta.pl', wishItems:  0},
      { id: 18, name: 'Wisława', surname: 'Szymborska ', email: 'Szymborska@poczta.pl', wishItems:  0},
      { id: 19, name: 'Mikołaj', surname: 'Kopernik ', email: 'Kopernik@poczta.pl', wishItems:  0},
      { id: 20, name: 'Maria', surname: 'Curie-Skłodowska', email: 'Curie-Skłodowska@poczta.pl', wishItems:  0},
      { id: 21, name: 'Robert', surname: 'Kubica', email: 'Kubica@poczta.pl', wishItems:  0},
    ];

    const itemsLost = [
      {id: 0, name: 'czerwony parasol', where: "Warszawa", when:         new Date("2016-01-11"), owner: users[0], description: "Bardzo piękny  czerwony parasol firmy Umbrella"  },
      {id: 1, name: 'zielona czapka', where: "Poznań", when:             new Date("2015-04-10"), owner: users[1], description: "Zielona czapka z daszkiem firmy Hut"  },
      {id: 2, name: 'sportowy but', where: "Kraków", when:               new Date("2014-05-05"), owner: users[2], description: "But nike do grania w piłkę nożną rozmiar 36"  },
      {id: 3, name: 'złoty telefon', where: "Bolecławiec", when:         new Date("2013-06-22"), owner: users[1], description: "Złoty telefon samsung s7 edge limited edition 2017" },
      {id: 4, name: 'samochod dacia', where: "Elbląg", when:             new Date("2015-08-25"), owner: users[8], description: "Na parkingu koło tesco. Nowa Dacia. 2017" },
      {id: 5, name: 'laptop Dell', where: "Szczecin", when:              new Date("2012-03-29"), owner: users[9], description: "Laptop z 1999 roku" },
      {id: 6, name: 'piesek Pimpek', where: "Piła", when:                new Date("2015-01-15"), owner: users[1], description: "Czarny piesek rasy bulldog. Dzieci tęsknią"},
      {id: 7, name: 'okulary Rajbany', where: "Chełm", when:             new Date("2014-08-05"), owner: users[6], description: "Moje ukochane okulary. Lekko porysowane"  },
      {id: 8, name: 'iphone X', where: "Kołobrzeg", when:                new Date("2013-02-27"), owner: users[7], description: "Nowy iphone. Jeszcze z foliami. Dla znalazcy nagroda"  },
      {id: 9, name: 'plecak szkolny', where: "Mielno", when:             new Date("2011-08-05"), owner: users[8], description: "Dziecko zgubiło różowy plecak idąc do podstawówki nr 2."  },
      {id: 10, name: 'rękawice bokserskie', where: "Międzyzdroje", when: new Date("2010-10-15"), owner: users[9], description: "Zgubione w klubie puncher" },
      {id: 11, name: 'piłka do kosza', where: "Zacisze", when:           new Date("2016-12-13"), owner: users[3], description: "Piłka firmy addidac. Kolor żółty. " },
      {id: 12, name: '1000zł', where: "Berlin", when:                    new Date("2016-02-01"), owner: users[2], description: "..." },
      {id: 13, name: 'krzesło ', where: "Nowy York", when:               new Date("2012-03-02"), owner: users[0], description: "Zaginione podczas przeprowadzki"},
    ];

    const wishItems = [
      { id: 0, user: [users[0], users[1],users[2]],            item: itemsFound[1], dateOfReservation: [new Date("2015-03-13"), new Date("2015-03-14"), new Date("2015-03-15")] },
      { id: 1, user: [users[7], users[1], users[2]], item: itemsFound[2], dateOfReservation: [new Date("2014-01-04"), new Date("2014-01-13"), new Date("2014-01-14")] },
    /*   { id: 2, user: [users[9], users[1], users[2], users[3], users[6], users[7]], item: itemsFound[3], dateOfReservation: [new Date("2014-01-01"), new Date("2014-01-04"), new Date("2014-01-13"), new Date("2014-01-14"), new Date("2014-01-15"), new Date("2014-01-17")] },
      { id: 3, user: [users[15], users[2], users[16], users[4], users[5], users[8]], item: itemsFound[4], dateOfReservation: [new Date("2014-01-01"), new Date("2014-01-02"), new Date("2014-01-04"), new Date("2014-01-13"), new Date("2014-01-14"), new Date("2014-01-16")] },
      { id: 4, user: [users[11], users[12], users[20]], item: itemsFound[5], dateOfReservation: [new Date("2014-01-04"), new Date("2014-01-13"), new Date("2014-01-14")] },
      { id: 5, user: [users[10], users[12], users[21]], item: itemsFound[6], dateOfReservation: [new Date("2014-01-09"), new Date("2014-01-13"), new Date("2014-01-14")] }, */
    ];


    const items = [
      {id: 0, name: 'czerwony parasol', where: "Warszawa", when:         new Date("2016-01-11"), description: "Bardzo piękny  czerwony parasol firmy Umbrella"  },
      {id: 1, name: 'zielona czapka', where: "Poznań", when:             new Date("2015-04-10"), description: "Zielona czapka z daszkiem firmy Hut"  },
      {id: 2, name: 'sportowy but', where: "Kraków", when:               new Date("2014-05-05"), description: "But nike do grania w piłkę nożną rozmiar 36"  },
      {id: 3, name: 'złoty telefon', where: "Bolecławiec", when:         new Date("2013-06-22"), description: "Złoty telefon samsung s7 edge limited edition 2017" },
      {id: 4, name: 'samochod dacia', where: "Elbląg", when:             new Date("2015-08-25"), description: "Na parkingu koło tesco. Nowa Dacia. 2017" },
      {id: 5, name: 'laptop Dell', where: "Szczecin", when:              new Date("2012-03-29"), description: "Laptop z 1999 roku" },
      {id: 6, name: 'piesek Pimpek', where: "Piła", when:                new Date("2015-01-15"), description: "Czarny piesek rasy bulldog. Dzieci tęsknią"},
      {id: 7, name: 'okulary Rajbany', where: "Chełm", when:             new Date("2014-08-05"), description: "Moje ukochane okulary. Lekko porysowane"  },
      {id: 8, name: 'iphone X', where: "Kołobrzeg", when:                new Date("2013-02-27"), description: "Nowy iphone. Jeszcze z foliami. Dla znalazcy nagroda"  },
      {id: 9, name: 'plecak szkolny', where: "Mielno", when:             new Date("2011-08-05"), description: "Dziecko zgubiło różowy plecak idąc do podstawówki nr 2."  },
      {id: 10, name: 'rękawice bokserskie', where: "Międzyzdroje", when: new Date("2010-10-15"), description: "Zgubione w klubie puncher" },
      {id: 11, name: 'piłka do kosza', where: "Zacisze", when:           new Date("2016-12-13"), description: "Piłka firmy addidac. Kolor żółty. " },
      {id: 12, name: '1000zł', where: "Berlin", when:                    new Date("2016-02-01"), description: "..." },
      {id: 13, name: 'krzesło ', where: "Nowy York", when:               new Date("2012-03-02"), description: "Zaginione podczas przeprowadzki"},
    ];
    
    const historyItems = [ 
      {id: 0,item: items[0], getBy: users[0], owner: true, when: new Date("2013-03-12")},
      {id: 1,item: items[1], getBy: users[1], owner: true, when: new Date("2012-01-02")},
      {id: 2,item: items[2], getBy: users[2], owner: true, when: new Date("2014-03-12")},
      {id: 3,item: items[3], getBy: users[3], owner: false, when: new Date("2016-07-03")},
      {id: 4,item: items[4], getBy: users[4], owner: true, when: new Date("2017-09-04")},
      {id: 5,item: items[5], getBy: users[5], owner: true, when: new Date("2015-06-22")},
      {id: 6,item: items[6], getBy: users[0], owner: false, when: new Date("2013-03-29")},
      {id: 7,item: items[7], getBy: users[0], owner: true, when: new Date("2014-04-16")},
      {id: 8,item: items[6], getBy: users[2], owner: false, when: new Date("2016-07-14")},
      {id: 9,item: items[7], getBy: users[0], owner: true, when: new Date("2010-10-13")},
      
      
      
    ]
    
    return { users, itemsFound, wishItems, itemsLost,historyItems };

  }
}
