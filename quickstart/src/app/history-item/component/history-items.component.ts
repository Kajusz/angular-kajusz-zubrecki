import { Component, OnInit } from '@angular/core';
import { Router }            from '@angular/router';

import { HistoryItem }                from '../entity/history-item';
import { HistoryItemsService }         from '../service/history-items.service';

import { User } from '../../user/user';
import { ItemFound } from '../../item/item-found';


@Component({
  selector: 'my-history-items',
  templateUrl: '../../../html/history-items.component.html',
  styleUrls: [ '../../../css/history-items.component.css' ]
})
export class HistoryItemsComponent implements OnInit {
  historyItems: HistoryItem[];
  selectedHistoryItem: HistoryItem;
  fromAtoZ: boolean = true;
  i: number;
  j: number;
  temp: HistoryItem;

  constructor(
    private historyItemService: HistoryItemsService,
    private router: Router) { }

  getItemsFromHistory(): void {
    this.historyItemService
        .getItemsFromHistory()
        .then(historyItems => this.historyItems = historyItems);
  }

 daysLeft(endDate: Date) {
   
/*    var a = moment(new Date());
   var endDateMoment = moment(endDate);
   var diffDays = endDateMoment.diff(a, 'days');
   alert(diffDays);
  return diffDays; */
  
 }

  delete(historyItem: HistoryItem): void {
    this.historyItemService
        .delete(historyItem.id)
        .then(() => {
          this.historyItems = this.historyItems.filter(h => h !== historyItem);
          if (this.selectedHistoryItem === historyItem) { this.selectedHistoryItem = null; }
        });
  }

  ngOnInit(): void {
    this.getItemsFromHistory();
  }

  onSelect(historyItem: HistoryItem): void {
    this.selectedHistoryItem = historyItem;
  }

  gotoDetail(historyItem: HistoryItem): void {
    this.selectedHistoryItem = historyItem;
    this.router.navigate(['/historyItem', this.selectedHistoryItem.id]);
}

sortByName(): void {

  this.fromAtoZ = !this.fromAtoZ;
  
          for (this.i = 0; this.i < this.historyItems.length; this.i++) {
              for (this.j = 0; this.j < this.historyItems.length - 1; this.j++) {
                  if ((this.historyItems[this.j].item.name > this.historyItems[this.j + 1].item.name) && this.fromAtoZ) {
                      this.temp = this.historyItems[this.j + 1];
                      this.historyItems[this.j + 1] = this.historyItems[this.j];
                      this.historyItems[this.j] = this.temp;
                  } else if ((this.historyItems[this.j].item.name < this.historyItems[this.j + 1].item.name) && !this.fromAtoZ) {
                      this.temp = this.historyItems[this.j + 1];
                      this.historyItems[this.j + 1] = this.historyItems[this.j];
                      this.historyItems[this.j] = this.temp;
                  }
              }
          }
      }
}
