import { ItemFound }      from '../../item/item-found';
import { User }      from '../../user/user';

export class HistoryItem {
  id: number;
  item: ItemFound;
  getBy: User;
  owner: string;
  when: Date;

  constructor(  id: number, item: ItemFound, getBy: User, owner: string, when: Date) {
    this.id = id;
    this.item = item;
    this.getBy = getBy;
    this.when = when;
    this.owner = owner;
  }
}


