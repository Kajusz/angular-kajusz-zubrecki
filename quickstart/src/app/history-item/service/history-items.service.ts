import { Injectable }    from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { ItemFound } from '../../item/item-found'

import { HistoryItem } from '../entity/history-item';

import { User } from '../../user/user';


@Injectable()
export class HistoryItemsService {

  private headers = new Headers({'Content-Type': 'application/json'});
  private historyItemsUrl = 'api/historyItems';  

  constructor(private http: Http) { }

  getItemsFromHistory(): Promise<HistoryItem[]> {
    return this.http.get(this.historyItemsUrl)
               .toPromise()
               .then(response => response.json().data as HistoryItem[])
               .catch(this.handleError);
  } 

  getHistoryItem(id: number ): Promise<HistoryItem> {
    const url = `${this.historyItemsUrl}/${id}`;
    return this.http.get(url)
      .toPromise()
      .then(response => response.json().data as HistoryItem)
      .catch(this.handleError);
  }

  delete(id: number ): Promise<void> {
    const url = `${this.historyItemsUrl}/${id}`;
    return this.http.delete(url, {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  create(item: ItemFound, getBy: User, owner: string, when: Date): Promise<HistoryItem> {
    return this.http
      .post(this.historyItemsUrl, JSON.stringify({item: item,getBy: getBy, owner: owner, when: when }), {headers: this.headers})
      .toPromise()
      .then(res => res.json().data as HistoryItem)
      .catch(this.handleError);
  }

  update(historyItem: HistoryItem ): Promise<HistoryItem> {
    const url = `${this.historyItemsUrl}/${historyItem.id}`;
    return this.http
      .put(url, JSON.stringify(historyItem), {headers: this.headers})
      .toPromise()
      .then(() => historyItem)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred in history items', error);
    return Promise.reject(error.message || error);
  }
}

