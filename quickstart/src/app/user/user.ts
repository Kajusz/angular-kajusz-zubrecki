import { Item } from '../item/item';
import { WishItem } from '../wishItem/wishItem';

export class User {
  id: number;
  name: string;
  surname: string;
  email: string;
  wishItems: number; 

  constructor (id: number, name: string, surname: string, email: string){
    this.id = id;
    this.name = name;
    this.surname = surname;
    this.email = email;
  }
}

