import { Component, OnInit } from '@angular/core';
import { Router }            from '@angular/router';

import { User }                from './user';
import { UserService }         from './user.service';

import { WishItem } from '../wishItem/wishItem';
import { WishItemService } from '../wishItem/wishItem.service';

@Component({
  selector: 'my-users',
  templateUrl: '../../html/users.component.html',
  styleUrls: [ '../../css/users.component.css' ]
})
export class UsersComponent implements OnInit {
  users: User[];
  selectedUser: User;
  errorMessage: string = "error";
  wishItems: WishItem[];

  fromAtoZ: boolean = true;
  i: number;
  j: number;
  temp: string;

  constructor(
    private userService: UserService,
    private wishItemService: WishItemService,
    private router: Router) { }

  getUsers(): void {
    this.userService
        .getUsers()
        .then(users => this.users = users);
  } 

  getWishItems(): void {
    this.wishItemService
        .getWishItems()
        .then(wishItems => this.wishItems = wishItems);
  }

  add(name: string, surname: string, email: string, wishItems: number): void {
    name = name.trim();
    if (!name || !surname || !email) {
      this.showInformation("All information are needed")
      return; }
    this.userService.create(name, surname, email, 0)
      .then(user => {
        this.users.push(user);
        this.selectedUser = null;
      });
  }

  showInformation(text: string) {
    document.getElementById('modal-body').innerHTML = "<br><br>" + text + "<br><br><br>";
    document.getElementById('myModal').style.display = "block";
  }
  
  closePopUp() {
    document.getElementById('myModal').style.display = "none";
  }

  deleteUserFromWishItems(user: User){
    for(let i = 0; i< this.wishItems.length; i++){
      for(let j = 0; j< this.wishItems[i].user.length; j++){
          if(this.wishItems[i].user[j].id === user.id){
            this.wishItems[i].user.splice(j, 1);
            this.wishItems[i].dateOfReservation.splice(j, 1);
            j = this.wishItems[i].user.length + 1;
          }
        }
        if(this.wishItems[i].user.length === 0){
          this.wishItemService.delete(this.wishItems[i].id);
        } else{
          this.wishItemService.update(this.wishItems[i]);
        }
      }
  }

  delete(user: User): void {
    this.deleteUserFromWishItems(user);
    this.userService
        .delete(user.id)
        .then(() => {
          this.users = this.users.filter(h => h !== user);
          if (this.selectedUser === user) { this.selectedUser = null; }
        });
  }

  ngOnInit(): void {
    this.getUsers();
    this.getWishItems();
  }

  onSelect(user: User): void {
    this.selectedUser = user;
  }

  gotoDetail(user: User): void {
    this.selectedUser = user;
    this.router.navigate(['/user', this.selectedUser.id]);
  }

  sortByName(): void {
           this.fromAtoZ = !this.fromAtoZ;
    
            for (this.i = 0; this.i < this.users.length; this.i++) {
                for (this.j = 0; this.j < this.users.length - 1; this.j++) {
                    if ((this.users[this.j].name > this.users[this.j + 1].name) && this.fromAtoZ) {
                        this.temp = this.users[this.j + 1].name;
                        this.users[this.j + 1].name = this.users[this.j].name;
                        this.users[this.j].name = this.temp;
                    } else if ((this.users[this.j].name < this.users[this.j + 1].name) && !this.fromAtoZ) {
                        this.temp = this.users[this.j + 1].name;
                        this.users[this.j + 1].name = this.users[this.j].name;
                        this.users[this.j].name = this.temp;
                    }
                }
            }
        }
}
