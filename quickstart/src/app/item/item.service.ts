import { Injectable }    from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Item } from './item';
import { ItemLost } from './item-lost';
import { ItemFound } from './item-found';

import { User } from '../user/user'

@Injectable()
export class ItemService {

  private headers = new Headers({'Content-Type': 'application/json'});
  private found: boolean;
  private itemsUrl: string;

  private itemsFoundUrl = 'api/itemsFound'; 
  private itemsLostUrl = 'api/itemsLost'; 

  constructor(private http: Http) { }

  getItemsFound(found: boolean): Promise<ItemFound[]> {
    this.foundOrLost(found);
    return this.http.get(this.itemsFoundUrl)
               .toPromise()
               .then(response => response.json().data as Item[])
               .catch(this.handleError);
  } 

  getItemsLost(found: boolean): Promise<ItemLost[]> {
    this.foundOrLost(found);
    return this.http.get(this.itemsLostUrl)
               .toPromise()
               .then(response => response.json().data as Item[])
               .catch(this.handleError);
  }

  getItem(id: number, found: boolean): Promise<Item> {
    this.foundOrLost(found);
    const url = `${this.itemsUrl}/${id}`;
    return this.http.get(url)
      .toPromise()
      .then(response => response.json().data as Item)
      .catch(this.handleError);
  }

  delete(id: number, found: boolean): Promise<void> {
    this.foundOrLost(found);
    const url = `${this.itemsUrl}/${id}`;
    return this.http.delete(url, {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  create(name: string, where: string, when: Date, description: string, image: string, found: boolean): Promise<Item> {
    this.foundOrLost(found);
    return this.http
      .post(this.itemsUrl, JSON.stringify({name: name,where: where,when:when, description: description,image: image }), {headers: this.headers})
      .toPromise()
      .then(res => res.json().data as Item)
      .catch(this.handleError);
  }

  createLost(name: string, where: string, when: Date, description: string, image: string, owner: User): Promise<ItemLost> {
    return this.http
      .post(this.itemsUrl, JSON.stringify({name: name,where: where,when:when, description: description,image: image, owner:owner}), {headers: this.headers})
      .toPromise()
      .then(res => res.json().data as ItemLost)
      .catch(this.handleError);
  }
  

  update(item: Item, found: boolean): Promise<Item> {
    this.foundOrLost(found);
    const url = `${this.itemsUrl}/${item.id}`;
    return this.http
      .put(url, JSON.stringify(item), {headers: this.headers})
      .toPromise()
      .then(() => item)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred in items', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

  private foundOrLost(found: boolean){
    if(found) {
      this.itemsUrl  = 'api/itemsFound';
    } else {
      this.itemsUrl  = 'api/itemsLost';
    }
  }
    
  

}

