import 'rxjs/add/operator/switchMap';
import { Component, OnInit }        from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Location }                 from '@angular/common';
import { Router } from '@angular/router';

import { User } from '../user/user';
import { UserService } from '../user/user.service';
 
import { Item }        from './item';
import { ItemFound } from './item-found';
import { ItemLost } from './item-lost';
import { ItemService } from './item.service';

@Component({
  selector: 'item-add-lost',
  templateUrl: '../../html/item-lost-add.component.html',
  styleUrls: [ '../../css/item-add.component.css' ]
})

export class ItemAddLostComponent implements OnInit  {
  itemLost: ItemLost;
  users: User [];
  itemsLost: ItemLost[];
  selectedItem: ItemLost;
  found: boolean;
  selectedUser: User;

  constructor(
    private itemService: ItemService,
    private userService: UserService,
    private route: ActivatedRoute,
    private location: Location,
    private router: Router
  ) {}

add(name: string, where: string,  date: Date, description: string, image: string): void {
  name = name.trim();
  if (!name || !where|| !description || !image) { 
    this.showInformation("You should insert all values");
    return; 
  }
  this.itemService.createLost(name, where, new Date(), description, image, this.selectedUser )
      .then(itemLost => {
          this.itemsLost.push(itemLost);
          
      });
      this.router.navigate(['/itemsLost']);
}

showInformation(text: string) {
  document.getElementById('modal-body').innerHTML = "<br><br>" + text + "<br><br><br>";
  document.getElementById('myModal').style.display = "block";
}

closePopUp() {
  document.getElementById('myModal').style.display = "none";
}

ngOnInit(): void {
  this.itemService.getItemsLost(false).then(itemsLost => this.itemsLost = itemsLost);
  this.userService.getUsers().then(users => this.users = users );
}

  goBack(): void {
    this.location.back();
  }
}
