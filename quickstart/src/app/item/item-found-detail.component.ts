import 'rxjs/add/operator/switchMap';
import { Component, OnInit }        from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Location }                 from '@angular/common';

import { Item }        from './item';
import { ItemService } from './item.service';

@Component({
  selector: 'item-found-detail',
  templateUrl: '../../html/item-found-detail.component.html',
  styleUrls: [ '../../css/item-detail.component.css' ]
})

export class ItemFoundDetailComponent implements OnInit {
  item: Item;
  found: boolean = true;

  constructor(
    private itemService: ItemService,
    private route: ActivatedRoute,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.route.paramMap
      .switchMap((params: ParamMap) => this.itemService.getItem(+params.get('id'), this.found))
      .subscribe(item => this.item = item);
  }

  save(): void {
    this.itemService.update(this.item, this.found)
      .then(() => this.goBack()); 
  }

  goBack(): void {
    this.location.back();
  }
}
