import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { Item } from './item';
import { ItemLost } from './item-lost';
import { ItemService } from './item.service';

declare var $: any;

@Component({
    selector: 'my-items-lost',
    templateUrl: '../../html/items-lost.component.html',
    styleUrls: ['../../css/items.component.css']
})
export class ItemsLostComponent implements OnInit {
    itemsLost: ItemLost[];
    selectedItem: Item;
    i: number;
    j: number;
    temp: ItemLost;
    fromAtoZ: boolean = true;
    private found: boolean = false;

    constructor(
        private itemService: ItemService,
        private router: Router) { }

        
        sortByName(): void {
            
            this.fromAtoZ  = !this.fromAtoZ;
            
            for (this.i = 0; this.i < this.itemsLost.length; this.i++) {
                for (this.j = 0; this.j < this.itemsLost.length - 1; this.j++) {
                    if ((this.itemsLost[this.j].name > this.itemsLost[this.j + 1].name) && this.fromAtoZ) {
                        this.temp = this.itemsLost[this.j + 1];
                        this.itemsLost[this.j + 1] = this.itemsLost[this.j];
                        this.itemsLost[this.j] = this.temp;
                    } else if ((this.itemsLost[this.j].name < this.itemsLost[this.j + 1].name) && !this.fromAtoZ) {
                        this.temp = this.itemsLost[this.j + 1];
                        this.itemsLost[this.j + 1] = this.itemsLost[this.j];
                        this.itemsLost[this.j] = this.temp;
                    }
                }
            }
        }
        
        delete(item: Item): void {
            this.itemService
            .delete(item.id, this.found)
            .then(() => {
                this.itemsLost = this.itemsLost.filter(h => h !== item);
                if (this.selectedItem === item) { this.selectedItem = null; }
            });
        }
        
        ngOnInit(): void {
            this.itemService.getItemsLost(false).then(itemsLost =>{
                this.itemsLost = itemsLost;
                this.activateSorting();
            });
        }

        
        onSelect(item: Item): void {
            this.selectedItem = item;
            
        }
        activateSorting(): void {
            $(function () {
                $('#foundItemTable').DataTable();
            });
        }
        
        gotoDetail(item: Item): void {
            this.selectedItem = item;
            this.router.navigate(['/itemLost', this.selectedItem.id]);
        }
    }
