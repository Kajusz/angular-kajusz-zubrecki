import { User } from '../user/user';

export class Item {
  id: number;
  name: string;
  where: string;
  when: Date;
  description: string;
  image: string; 
  owner: User;
  onWishList: boolean;

  constructor(id: number, name: string, where: string, when: Date, description: string, image: string) {
    this.id = id;
    this.name = name;
    this.where = where;
    this.when = when;
    this.description = description;
    this.image = image;
  }
}


