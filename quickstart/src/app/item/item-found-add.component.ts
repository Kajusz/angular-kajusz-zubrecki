import 'rxjs/add/operator/switchMap';
import { Component, OnInit }        from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Location }                 from '@angular/common';
import { Router }                   from '@angular/router';

import { Item }                     from './item';
import { ItemFound }                from './item-found';
import { ItemLost }                 from './item-lost';
import { ItemService }              from './item.service';

@Component({
  selector: 'item-add-found',
  templateUrl: '../../html/item-found-add.component.html',
  styleUrls: [ '../../css/item-add.component.css' ]
})

export class ItemAddFoundComponent  {
  item: Item;
  itemsFound: ItemFound[];
  selectedItem: Item;
  found: boolean;

  constructor(
    private itemService: ItemService,
    private route: ActivatedRoute,
    private location: Location,
    private router: Router
  ) {}
 /*  add(name: string, where: string, date: Date, description: string, image: string): void { */
  add(name: string, where: string, description: string, image: string): void {
    name = name.trim();
    let date = new Date();
    if (!name || !where   || !description || !image) {
      this.showInformation("You should insert all values");
      return; }
    this.itemService.create(name, where, date, description, image, true)
        .then(item => {
            this.itemsFound.push(item);
            this.selectedItem = null;
        });
        this.router.navigate(['/itemsFound']);
}

showInformation(text: string) {
  document.getElementById('modal-body').innerHTML = "<br><br>" + text + "<br><br><br>";
  document.getElementById('myModal').style.display = "block";
}

closePopUp() {
  document.getElementById('myModal').style.display = "none";
}

 ngOnInit(): void {
  this.itemService.getItemsFound(true).then(itemsFound => this.itemsFound = itemsFound);
  }

  goBack(): void {
    this.location.back();
  }
}
