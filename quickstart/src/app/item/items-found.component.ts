import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { Item } from './item';
import { ItemFound } from './item-found';
import { ItemService } from './item.service';

import { User } from '../user/user';
import { UserService } from '../user/user.service';

import { WishItem } from '../wishItem/wishItem';
import { WishItemService } from '../wishItem/wishItem.service';

declare var $: any;

@Component({
    selector: 'my-items-found',
    templateUrl: '../../html/items-found.component.html',
    styleUrls: ['../../css/items.component.css', '../../css/modal.css']
})
export class ItemsFoundComponent implements OnInit {
    itemsFound: ItemFound[];
    selectedItem: Item;
    selectedUser: User;
    users: User[];
    wishItemUsers: User[] = [];
    wishItemDates: Date[] = [];
    i: number;
    j: number;
    temp: Item;
    fromAtoZ: boolean = true;
    private found: boolean = true;

    constructor(
        private userService: UserService,
        private wishItemService: WishItemService,
        private itemService: ItemService,
        private router: Router) { }

    getItems(): void {
        this.itemService.getItemsFound(true).then(itemsFound => this.itemsFound = itemsFound);
    }

    addItemToWishItems(selectedItem: Item) {
        this.selectedItem = selectedItem;
        this.selectedUser = this.users[1];
        this.checkIfItemIsAlreadyOnWishList();
    }


    addWishItem(selectedUser: User) {
        if (this.addUserReservation(selectedUser)){
            this.createWishItemAndSave(selectedUser);
            this.gotoWishItems();
            this.setFlagThatItemIsOnWishList();
            document.getElementById('myModal').style.display = "none";
        }
    }

    checkIfItemIsAlreadyOnWishList() {
        if (this.selectedItem.onWishList === false || this.selectedItem.onWishList === undefined) {
            document.getElementById('myModal').style.display = "block";
        } else {
            this.showInformation("Item already on list");
        }
    }

    createWishItemAndSave(selectedUser: User) {
        this.wishItemUsers.push(selectedUser);
        this.wishItemDates.push(new Date());
        this.wishItemService.create(this.wishItemUsers, this.selectedItem, this.wishItemDates);
    }

    addUserReservation(selectedUser: User) {
        if (this.checkIfUserHasFreeReservations(selectedUser)) {
            selectedUser.wishItems++;
            this.userService.update(selectedUser);
            return true;
        } else {
            return false;
        }
    }

    checkIfUserHasFreeReservations(selectedUser: User) {
        if (this.selectedUser.wishItems > 2) {
            document.getElementById('myModal').style.display = "none";
            this.showInformation("User had already reserved 3 items");
            return false;
        } else {
            return true;
        }
    }

    setFlagThatItemIsOnWishList() {
        this.selectedItem.onWishList = true;
        this.itemService.update(this.selectedItem, true);
    }

    sortByName(): void {
        this.fromAtoZ = !this.fromAtoZ;

        for (this.i = 0; this.i < this.itemsFound.length; this.i++) {
            for (this.j = 0; this.j < this.itemsFound.length - 1; this.j++) {
                if ((this.itemsFound[this.j].name > this.itemsFound[this.j + 1].name) && this.fromAtoZ) {
                    this.temp = this.itemsFound[this.j + 1];
                    this.itemsFound[this.j + 1] = this.itemsFound[this.j];
                    this.itemsFound[this.j] = this.temp;
                } else if ((this.itemsFound[this.j].name < this.itemsFound[this.j + 1].name) && !this.fromAtoZ) {
                    this.temp = this.itemsFound[this.j + 1];
                    this.itemsFound[this.j + 1] = this.itemsFound[this.j];
                    this.itemsFound[this.j] = this.temp;
                }
            }
        }
    }

    delete(item: Item): void {
        this.itemService
            .delete(item.id, true)
            .then(() => {
                this.itemsFound = this.itemsFound.filter(h => h !== item);
                if (this.selectedItem === item) { this.selectedItem = null; }
            });
    }

    ngOnInit(): void {
        this.itemService.getItemsFound(true).then(foundItems => {
            this.itemsFound = foundItems;
            this.activateSorting();
        });
        this.getUsers();
    }

    activateSorting(): void {
        $(function () {
            $('#foundItemTable').DataTable();
        });
    }

    getUsers(): void {
        this.userService
            .getUsers()
            .then(users => this.users = users);
    }

    onSelect(item: Item): void {
        this.selectedItem = item;
    }

    closePopUp() {
        document.getElementById('myModal').style.display = "none";
        document.getElementById('infoModel').style.display = "none";
    }

    gotoDetail(item: Item): void {
        this.selectedItem = item;
        this.router.navigate(['/itemFound', this.selectedItem.id]);
    }

    showInformation(text: string){
        document.getElementById('infoBody').innerHTML = "<br><br>" + text + "<br><br><br>";
        document.getElementById('infoModel').style.display = "block";
      }


    gotoWishItems(): void {
        this.router.navigate(['/wishItems']);
    }
}
